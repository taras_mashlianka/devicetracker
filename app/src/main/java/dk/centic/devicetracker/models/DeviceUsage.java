package dk.centic.devicetracker.models;

public class DeviceUsage {

    private int id;
    private String userId;
    private String packageName;
    private String usageTime;
    private String usageType;
    private Boolean isSent;

    public DeviceUsage(){}

    public DeviceUsage(int id, String userId, String packageName, String usageTime) {
        setId(id);
        setUserId(userId);
        setPackageName(packageName);
        setUsageTime(usageTime);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getUsageTime() {
        return usageTime;
    }

    public void setUsageTime(String usageTime) {
        this.usageTime = usageTime;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public Boolean getSent() {
        return isSent;
    }

    public void setSent(Integer sent) {
        isSent = sent > 0;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }
}
