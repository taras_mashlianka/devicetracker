package dk.centic.devicetracker.models;

public class User {
    private String id;
    private String name;
    private Boolean isActive;

    public User(){}

    public User(String id, String name, Integer isActive) {
        setId(id);
        setName(name);
        setIsActive(isActive);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive > 0;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
