package dk.centic.devicetracker.db.repositories;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import dk.centic.devicetracker.db.DbHelper;
import dk.centic.devicetracker.models.DeviceUsage;

public class DeviceUsageRepository extends Repository implements IRepository {

    private static final String LOG_TAG = "Device usage table";

    private static final String DEVICE_USAGE_TABLE_NAME = "DEVICE_USAGE";

    private static final String KEY_ID = "_id";
    private static final String PRIMARY_KEY = KEY_ID;
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_PACKAGE_NAME = "package_name";
    private static final String KEY_USAGE_TIME = "usage_time";
    private static final String KEY_USAGE_TYPE = "usage_type";
    private static final String KEY_IS_SENT = "is_sent";

    public DeviceUsageRepository(Context context) {
        super(context);
        DbHelper.repositoryList.add(this);
    }

    @Override
    public RepositoryInfo getRepositoryInfo() {
        RepositoryInfo repositoryInfo = new RepositoryInfo();

        repositoryInfo.setTableName(DEVICE_USAGE_TABLE_NAME);
        repositoryInfo.setPrimaryKeyName(PRIMARY_KEY);

        Map<String, String> tableFields = new HashMap<>();
        tableFields.put(KEY_ID, DbHelper.TYPE_NUMERIC);
        tableFields.put(KEY_USER_ID, DbHelper.TYPE_TEXT);
        tableFields.put(KEY_PACKAGE_NAME, DbHelper.TYPE_TEXT);
        tableFields.put(KEY_USAGE_TYPE, DbHelper.TYPE_TEXT);
        tableFields.put(KEY_IS_SENT, DbHelper.TYPE_NUMERIC);

        repositoryInfo.setFields(tableFields);
        return repositoryInfo;
    }

    private DeviceUsage fromCursor(Cursor cursor) {
        // TODO: Create DeviceUsage object based on cursor data
        return new DeviceUsage();
    }

    public boolean createUsageReport(DeviceUsage report) {
        // TODO: create new db entry from device usage
        return false;
    }

    public Collection<DeviceUsage> getUnsynchronizedReports() {
        // TODO: Return all unsynced reports
        List<DeviceUsage> unsyncedReports = new LinkedList<>();
        return unsyncedReports;
    }
}
