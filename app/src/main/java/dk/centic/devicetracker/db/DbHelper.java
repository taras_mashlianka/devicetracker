package dk.centic.devicetracker.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import dk.centic.devicetracker.db.repositories.IRepository;
import dk.centic.devicetracker.db.repositories.RepositoryInfo;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "UsersDB";
    private static final int DATABASE_VERSION = 1;

    //Types
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_NUMERIC = "numeric";

    //Table list
    public static List<IRepository> repositoryList = new ArrayList<>();

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (IRepository table : repositoryList) {
            RepositoryInfo repositoryInfo = table.getRepositoryInfo();
            String execSQLQuery = "create table " + repositoryInfo.getTableName() +" (";
            for ( Map.Entry<String, String> entry : repositoryInfo.getFields().entrySet() ) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (Objects.equals(key, repositoryInfo.getPrimaryKeyName())){
                    execSQLQuery += key + " " + value + " primary key,";
                } else{
                    execSQLQuery += key + " " + value + ",";
                }
            }
            if (execSQLQuery.endsWith(",")) {
                execSQLQuery = execSQLQuery.substring(0, execSQLQuery.length() - 1);
            }
            execSQLQuery += ");";
            db.execSQL(execSQLQuery);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (IRepository repository : repositoryList) {
            RepositoryInfo repositoryInfo = repository.getRepositoryInfo();
            db.execSQL("DROP TABLE IF EXISTS " + repositoryInfo.getTableName());
        }
        this.onCreate(db);
    }
}
