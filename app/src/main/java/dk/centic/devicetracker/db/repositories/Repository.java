package dk.centic.devicetracker.db.repositories;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import dk.centic.devicetracker.db.DbHelper;

public class Repository {

    DbHelper dbHelper;

    Repository(){}

    public Repository(Context context){
        dbHelper = new DbHelper(context);
    }

    boolean isRepositoryEmpty(String tableName){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(tableName, null, null, null, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        return count <= 0;
    }

    boolean checkIfRowExists(String tableName, String dbField, String fieldValue) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(tableName, null, dbField + " = ?", new String[] {fieldValue}, null, null, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}
