package dk.centic.devicetracker;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.crashlytics.android.Crashlytics;
import com.google.zxing.integration.android.IntentIntegrator;

import dk.centic.devicetracker.receivers.DeviceUsageReceiver;
import dk.centic.devicetracker.services.UIService;
import io.fabric.sdk.android.Fabric;

import static dk.centic.devicetracker.R.id.buttonScan;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    UIService uiService;
    DeviceUsageReceiver usageReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        setTitle(getString(R.string.label_user_list));

        ListView listView = (ListView) findViewById(R.id.listViewUsers);

        uiService = new UIService(this, listView);
        uiService.updateUserList();

        usageReceiver = new DeviceUsageReceiver();
        IntentFilter screenOnTimeFilter = new IntentFilter();
        screenOnTimeFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenOnTimeFilter.addAction(Intent.ACTION_SCREEN_OFF);
        screenOnTimeFilter.addAction(Intent.ACTION_USER_PRESENT);
        registerReceiver(usageReceiver, screenOnTimeFilter);

        /*Intent accessibilitySettingsIntent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
        startActivityForResult(accessibilitySettingsIntent, 0);*/

        FloatingActionButton buttonScan = (FloatingActionButton) findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(usageReceiver);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        uiService.startScanning();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (uiService.іsQRScanningSuccessful(IntentIntegrator.parseActivityResult(requestCode, resultCode, data))) {
            uiService.openAddUserDialog();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
