package dk.centic.devicetracker.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import java.util.ArrayList;
import java.util.List;

import dk.centic.devicetracker.models.User;
import dk.centic.devicetracker.R;
import dk.centic.devicetracker.services.UserService;
import layout.UserListWidget;

public class ListAdapter implements RemoteViewsFactory {

    private List<String> userNames;
    private List<User> userList;
    private Context context;

    private UserService userService;

    public ListAdapter(Context context) {
        this.context = context;
        userService = new UserService(context);
    }

    @Override
    public void onCreate() {
        userNames = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return userNames.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.user_list_item);
        remoteViews.setTextViewText(R.id.tvItemText, userNames.get(position));

        User currentUser = userList.get(position);
        if (currentUser.getIsActive()){
            remoteViews.setInt(R.id.tvItemTextBackground, "setBackgroundColor", R.color.colorWidgetActiveUserBackground);

        } else {
            remoteViews.setInt(R.id.tvItemTextBackground, "setBackgroundColor", Color.TRANSPARENT);
        }
        Intent clickIntent = new Intent();
        clickIntent.putExtra(UserListWidget.ITEM_USER_ID, currentUser.getId());
        remoteViews.setOnClickFillInIntent(R.id.tvItemText, clickIntent);
        return remoteViews;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onDataSetChanged() {
        userNames.clear();
        userList = userService.getUsers();
        userNames = userService.getUserNames();
    }

    @Override
    public void onDestroy() {

    }
}
