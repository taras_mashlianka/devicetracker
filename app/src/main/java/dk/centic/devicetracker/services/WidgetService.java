package dk.centic.devicetracker.services;

import android.content.Intent;
import android.widget.RemoteViewsService;

import dk.centic.devicetracker.adapters.ListAdapter;

public class WidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListAdapter(getApplicationContext());
    }
}
