package dk.centic.devicetracker.services;

import android.content.Context;

import dk.centic.devicetracker.db.repositories.DeviceUsageRepository;

public class DeviceUsageService {
    private DeviceUsageRepository deviceUsageRepository;

    public DeviceUsageService(Context context){
        deviceUsageRepository = new DeviceUsageRepository(context);
    }
}
