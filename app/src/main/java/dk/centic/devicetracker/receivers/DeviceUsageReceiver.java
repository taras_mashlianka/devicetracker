package dk.centic.devicetracker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Date;

import dk.centic.devicetracker.services.ActiveApplicationMonitorService;

public class DeviceUsageReceiver extends BroadcastReceiver {

    private static final String TAG = "DeviceUsageReceiver";

    public static final String ACTION_APP_CHANGED = "dk.centic.devicetracker.intent.action.ACTION_APP_CHANGED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null)
            return;

        String intentAction = intent.getAction();

        if (intentAction.equals(Intent.ACTION_SCREEN_ON)) {
            Log.i(TAG, String.format("Screen has been turned on at %d", new Date().getTime()));

        } else if (intentAction.equals(Intent.ACTION_SCREEN_OFF)) {
            Log.i(TAG, String.format("Screen turned off at %d", new Date().getTime()));

        } else if (intentAction.equals(Intent.ACTION_USER_PRESENT)) {
            Log.i(TAG, String.format("User unlocked device at %d", new Date().getTime()));

        } else if (intentAction.equals(ACTION_APP_CHANGED)) {
            String packageName = intent.getStringExtra(ActiveApplicationMonitorService.EXTRA_PACKAGE_NAME);
            Log.i(TAG, String.format("User has changed active application to %s at %d", packageName, new Date().getTime()));
        }
    }
}
